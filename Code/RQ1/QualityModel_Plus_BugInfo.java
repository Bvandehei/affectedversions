import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.Map;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import java.time.LocalDate;
import java.time.LocalDateTime;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

public class QualityModel_Plus_BugInfo {
   public static HashMap<String, String> closedBugsAndDirectory;
   public static HashMap<String, String> closedBugsAndHash;
   public static HashMap<String, String> closedBugsAndFix;
   public static HashMap<String, LocalDateTime> closedBugsAndFixDate;
   public static HashMap<String, String> closedBugsAndClasses;
   public static HashMap<String, String> closedBugsAndResolution;
   public static HashMap<LocalDateTime, String> releaseNames;
   public static HashMap<LocalDateTime, String> releaseID;

   public static ArrayList<LocalDateTime> releases;
   public static HashMap<String, Integer> closedBugs;
   public static HashMap<String, Integer> closedBugsAndAV;
   public static HashMap<String, Integer> closedBugsAndCreation;
   public static HashMap<String, String> closedBugsAndCreationDate;
   public static Double hasAffectVersionAfterCreation;
   public static Double noAffectVersion;
   public static Double bugWithProblem;
   public static Double closedAndLinkedBugs;
   public static Integer numVersions;

  //Adds a release to the arraylist
   public static void addRelease(String strDate, String name, String id) {
      LocalDate date = LocalDate.parse(strDate);
      LocalDateTime dateTime = date.atStartOfDay();
      if (!releases.contains(dateTime))
         releases.add(dateTime);
      releaseNames.put(dateTime, name);
      releaseID.put(dateTime, id);
      return;
   }

   // Finds the release corresponding to a creation or resolution date
   public static Integer findRelease(String strDate) {
      LocalDateTime dateTime = LocalDateTime.parse(strDate);
      int i;
      int index = 0;
      for (i = 0; i < releases.size(); i++ ) {
         // if date is after this release, the set to next index
         if (dateTime.isAfter(releases.get(i)))
            index = i + 1;
      }
      //acount for 0 offset
      return index + 1;
   }

   //Finds the exact release for affect versions
   public static Integer findExactRelease(String strDate) {
      LocalDate date = LocalDate.parse(strDate);
      LocalDateTime dateTime = date.atStartOfDay();
      int i = releases.indexOf(dateTime);
      if ( i == -1) {
         System.out.println("Did not find exact release for date " + strDate);
         System.exit(-1);
      }
      //account for 0 offset
      return i + 1;
   }

   private static String readAll(Reader rd) throws IOException {
      StringBuilder sb = new StringBuilder();
      int cp;
      while ((cp = rd.read()) != -1) {
         sb.append((char) cp);
      }
      return sb.toString();
   }

   public static JSONArray readJsonArrayFromUrl(String url) throws IOException, JSONException {
      InputStream is = new URL(url).openStream();
      try {
         BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
         String jsonText = readAll(rd);
         JSONArray json = new JSONArray(jsonText);
         return json;
       } finally {
         is.close();
       }
   }

   public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
      InputStream is = new URL(url).openStream();
      try {
         BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
         String jsonText = readAll(rd);
         JSONObject json = new JSONObject(jsonText);
         return json;
       } finally {
         is.close();
       }
   }


   //Fills the arraylist with releases dates and orders them
   //Ignores releases with missing dates
   public static void getReleases(String projName) throws IOException, JSONException  {
         Integer i;
         String url = "https://issues.apache.org/jira/rest/api/2/project/" + projName;
         JSONObject json = readJsonFromUrl(url);
         JSONArray versions = json.getJSONArray("versions");
         releaseNames = new HashMap<LocalDateTime, String>();
         releaseID = new HashMap<LocalDateTime, String> ();
         for (i = 0; i < versions.length(); i++ ) {
            String name = "";
            String id = "";
            if(versions.getJSONObject(i).has("releaseDate")) {
               if (versions.getJSONObject(i).has("name"))
                  name = versions.getJSONObject(i).get("name").toString();
               if (versions.getJSONObject(i).has("id"))
                  id = versions.getJSONObject(i).get("id").toString();
               addRelease(versions.getJSONObject(i).get("releaseDate").toString(),
                          name,id);
            }
         }
         // order releases by date
         Collections.sort(releases, new Comparator<LocalDateTime>(){
            @Override
            public int compare(LocalDateTime o1, LocalDateTime o2) {
                return o1.compareTo(o2);
            }
         });
         if (releases.size() < 6)
            return;
         FileWriter fileWriter = null;
	 try {
            fileWriter = null;
            String outname = projName + "VersionInfo.csv";
				    //Name of CSV for output
				    fileWriter = new FileWriter(outname);
            fileWriter.append("Index,Version ID,Version Name,Date");
            fileWriter.append("\n");
            numVersions = releases.size();
            for ( i = 0; i < releases.size(); i++) {
               Integer index = i + 1;
               fileWriter.append(index.toString());
               fileWriter.append(",");
               fileWriter.append(releaseID.get(releases.get(i)));
               fileWriter.append(",");
               fileWriter.append(releaseNames.get(releases.get(i)));
               fileWriter.append(",");
               fileWriter.append(releases.get(i).toString());
               fileWriter.append("\n");
            }

         } catch (Exception e) {
            System.out.println("Error in csv writer");
            e.printStackTrace();
         } finally {
            try {
               fileWriter.flush();
               fileWriter.close();
            } catch (IOException e) {
               System.out.println("Error while flushing/closing fileWriter !!!");
               e.printStackTrace();
            }
         }
         return;
   }

   public static void getBugInfo(String projName) throws IOException, JSONException {
          Integer j = 0, i = 0, total = 1, k = 0;
      //Get JSON API for closed bugs w/ AV in the project
      do {
         //Only gets a max of 1000 at a time, so must do this multiple times if bugs >1000
         j = i + 1000;
         String url = "https://issues.apache.org/jira/rest/api/2/search?jql=project=%22"
                + projName + "%22AND%22issueType%22=%22Bug%22AND(%22status%22=%22closed%22OR"
                + "%22status%22=%22resolved%22)AND%22resolution%22=%22fixed%22&fields=key,resolutiondate,versions,created&startAt="
                + i.toString() + "&maxResults=" + j.toString();
         JSONObject json = readJsonFromUrl(url);
         JSONArray issues = json.getJSONArray("issues");
         total = json.getInt("total");
         for ( i = i; i < total && i < j; i++) {
            //Iterate through each bug
            String key = issues.getJSONObject(i%1000).get("key").toString();
            closedBugs.put(key, null);
            //Get creation date corresponding release indexes
            Integer created = findRelease(issues.getJSONObject(i%1000)
                         .getJSONObject("fields").get("created").toString().substring(0,20));
            closedBugsAndCreation.put(key,created);
            String creationdate = issues.getJSONObject(i%1000)
                         .getJSONObject("fields").get("created").toString();
            creationdate = creationdate.replace("T", " ").replace(".000", " ");
            closedBugsAndCreationDate.put(key,creationdate);
            String resolution = issues.getJSONObject(i%1000)
                         .getJSONObject("fields").get("resolutiondate").toString();
            closedBugsAndResolution.put(key,resolution.replace("T", " ").replace(".000", " "));
            //Get list of AV recorded for the bug
            if( issues.getJSONObject(i%1000).getJSONObject("fields").has("versions")) {
               JSONArray versions = issues.getJSONObject(i%1000).getJSONObject("fields")
                .getJSONArray("versions");
               closedBugsAndAV.put(key, null);
               for ( k = 0; k < versions.length(); k++) {
                  if (versions.getJSONObject(k).has("releaseDate")) {
                     Integer x = findExactRelease(versions.getJSONObject(k)
                         .get("releaseDate").toString());
                     if ( closedBugsAndAV.get(key) == null || x < closedBugsAndAV.get(key)) {
                        closedBugsAndAV.put(key, x);
                     }
                  }
               }
            }
            else {
               closedBugsAndAV.put(key, null);
            }
         }
      } while (i < total);
      return;
   }

   public static void main(String[] args) throws IOException, JSONException {
      numVersions=0;
      String csvFile = "ProjectsAndUrlsEditted.csv";
      String cvsSplitBy = ",";
      Integer i, j, t;
      try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
         FileWriter fileWriter = null;
         FileWriter fileWriter2 = null;
				 try {
				    String linefromcsv, newline, write;
				    //Name of CSV for output
				    fileWriter = new FileWriter("QualityModel.csv");
				    fileWriter2 = new FileWriter("Projects.csv");
				    //Header for CSV
				    fileWriter.append("Key,Number of C/L Bugs w/ No Probs,Number of Releases,"
              + "Number of Closed and Linked Bugs," +
              "Number of C/L Bugs with no AV,Percent of C/L Bugs with no AV," +
              "Number of C/L Bugs with First AV after Ticket Creation,Percent of C/L Bugs with First AV after Ticket Creation," +
              "Number of C/L Bugs with a Problem,Percent of C/L Bugs with a Problem");
				    fileWriter.append("\n");
            fileWriter2.append("Jira Key, Git URL\n");


            br.readLine();
            //reade from csv one project at a time
            while ((linefromcsv = br.readLine()) != null ) {
               String[] token = linefromcsv.split(cvsSplitBy);
               if ( token.length > 2  ) {
                  releases = new ArrayList<LocalDateTime>();
                  getReleases(token[0]);

                  closedBugs = new HashMap<String, Integer>();
                  closedBugsAndFix = new HashMap<String, String>();
						      closedBugsAndFixDate = new HashMap<String, LocalDateTime>();
						      closedBugsAndClasses = new HashMap<String, String>();
                  closedBugsAndDirectory = new HashMap<String, String>();
						      closedBugsAndCreationDate = new HashMap<String, String>();
						      closedBugsAndHash = new HashMap<String, String>();
                  closedBugsAndAV = new HashMap<String, Integer>();
                  closedBugsAndCreation = new HashMap<String, Integer>();
                  closedBugsAndResolution = new HashMap<String, String>();
                  getBugInfo(token[0]);
                  ArrayList<String> bugs = new ArrayList<String>(closedBugs.keySet());

                  HashMap<String, Integer> repoToNumCLBugs = new HashMap<String, Integer>();
                  //check all repos for the bug key
                  for ( i = 2; i < token.length; i++) {
                     String command = "git clone " + token[i];
                     Process proc = Runtime.getRuntime().exec(command);
                     proc.waitFor();
                     String[] token2 = token[i].split("/");
                     String repoURL = token[i];
                     repoToNumCLBugs.put(repoURL, 0);
                     String path =  token2[token2.length - 1].substring(0, token2[token2.length - 1].length() - 4);
                     for ( j = 0; j < bugs.size(); j++) {
                        command = "git --git-dir ./" + path +
                         "/.git log --branches --grep="+ bugs.get(j) +"[^0-9]\\|" + bugs.get(j)  + "$ --pretty=format:%cd%H --date=iso -1";
                        // Read the output
                        proc = Runtime.getRuntime().exec(command);
                        BufferedReader reader =
                           new BufferedReader(new InputStreamReader(proc.getInputStream()));
                        String line = "";
                      if((line = reader.readLine()) != null) {
                          proc.waitFor();
                          repoToNumCLBugs.put(repoURL, repoToNumCLBugs.get(repoURL) + 1);
                      }
                      else {
                        proc.waitFor();
                      }
                    }
                    command = "rm -rf " + path;
                    proc = Runtime.getRuntime().exec(command);
                    proc.waitFor();
                  }
                  Map.Entry<String, Integer> maxEntry = null;

                  for (Map.Entry<String, Integer> entry : repoToNumCLBugs.entrySet()){
                     if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0){
                        maxEntry = entry;
                     }
                  }
                  fileWriter2.append(token[0] + "," + maxEntry.getKey() + "\n");

                  //check all repos for the bug key
                  HashMap <String, String> bugToFixes = new HashMap<String, String>();
                  String command = "git clone " + maxEntry.getKey();
                  Process proc = Runtime.getRuntime().exec(command);
                  proc.waitFor();
                  String[] token2 = maxEntry.getKey().split("/");
                  String path =  token2[token2.length - 1].substring(0, token2[token2.length - 1].length() - 4);
                  for ( j = 0; j < bugs.size(); j++) {
                     command = "git --git-dir ./" + path +
                         "/.git log --branches --grep="+ bugs.get(j) +"[^0-9]\\|" + bugs.get(j)  + "$ --pretty=format:%cd%H --date=iso-strict ";
                        // Read the output
                     proc = Runtime.getRuntime().exec(command);
                     BufferedReader reader =
                           new BufferedReader(new InputStreamReader(proc.getInputStream()));
                     String line = "";
                     String fixes = "";
                     while((line = reader.readLine()) != null) {
                         fixes += line + "\n";
                     }
                     bugToFixes.put(bugs.get(j), fixes);

                     proc.waitFor();
                     command = "git --git-dir ./" + path +
                         "/.git log --branches --grep="+ bugs.get(j) +"[^0-9]\\|" + bugs.get(j)  + "$ --pretty=format:%cd%H --date=iso -1";
                        // Read the output
                     proc = Runtime.getRuntime().exec(command);
                     reader =
                           new BufferedReader(new InputStreamReader(proc.getInputStream()));
                     line = "";
                     if((line = reader.readLine()) != null) {
                          proc.waitFor();
                          String datePart = line.substring(0, 25);
                          String hash = line.substring(25);
                          Integer x = findRelease(datePart.substring(0,19).replace(" ", "T"));
                          LocalDateTime dt = LocalDateTime.parse(datePart.substring(0,19).replace(" ", "T"));
                          if (closedBugs.get(bugs.get(j)) == null || closedBugs.get(bugs.get(j)) < x
                               || closedBugsAndFixDate.get(bugs.get(j)).isBefore(dt)) {
                               closedBugsAndFix.put(bugs.get(j), datePart);
                               closedBugsAndFixDate.put(bugs.get(j), dt);
                               closedBugsAndHash.put(bugs.get(j), hash);
                               closedBugsAndDirectory.put(bugs.get(j), path);
                               closedBugs.put(bugs.get(j), x);
                               command = "git --git-dir ./" + path + "/.git " +
                                 "diff --name-only " + hash + "^ " + hash;
                               proc = Runtime.getRuntime().exec(command);
                               reader =
				                         new BufferedReader(new InputStreamReader(proc.getInputStream()));
                               line = "";
                               String classes = "";
                               while((line = reader.readLine()) != null) {
                                  classes += line + "|";
                               }
                               proc.waitFor();
                               closedBugsAndClasses.put(bugs.get(j), classes);
                          }
                      }
                      else {
                        proc.waitFor();
                      }
                   }
                        command = "rm -rf " + path;
                        // Read the output
                        proc = Runtime.getRuntime().exec(command);
                        proc.waitFor();

                  hasAffectVersionAfterCreation = 0.0;
						      noAffectVersion = 0.0;
						      bugWithProblem = 0.0;
                  closedAndLinkedBugs = 0.0;
                  for ( j = 0; j < bugs.size(); j++) {
                     if ( closedBugs.get(bugs.get(j)) != null && !(closedBugsAndAV.get(bugs.get(j)) != null &&
                            closedBugsAndAV.get(bugs.get(j)) == closedBugs.get(bugs.get(j)) &&
                            closedBugs.get(bugs.get(j)) == closedBugsAndCreation.get(bugs.get(j)))) {
                        closedAndLinkedBugs++;
                        if (closedBugsAndAV.get(bugs.get(j)) == null) {
                           noAffectVersion++;
                           bugWithProblem++;
                        }
                        else if (closedBugsAndAV.get(bugs.get(j)) > closedBugsAndCreation.get(bugs.get(j))){
                           hasAffectVersionAfterCreation++;
                           bugWithProblem++;
                        }
                     }
                  }
                  Double noProb = closedAndLinkedBugs - bugWithProblem;

               		write = token[0] + "," + noProb.toString()+ ","+ numVersions.toString()+
                          ","+ closedAndLinkedBugs.toString() + ","
                          + noAffectVersion.toString() + ",";
                  Double percent = noAffectVersion/closedAndLinkedBugs;
               		write += percent.toString()+ "," + hasAffectVersionAfterCreation.toString() + ",";
                  percent = hasAffectVersionAfterCreation/closedAndLinkedBugs;
               		write += percent.toString() + "," + bugWithProblem.toString() +",";
                  percent = bugWithProblem/closedAndLinkedBugs;
               		write += percent.toString() + "\n";
               		fileWriter.append(write);
                  System.out.println(write);

                  if (noProb >= 100 && numVersions >= 6) {

                     FileWriter bugWriter = null;
				             try {
                        bugWriter = null;
                        String outname = token[0] + "BugInfo.csv";
				                //Name of CSV for output
				                bugWriter = new FileWriter(outname);
				                //Header for CSV
				                bugWriter.append("Bug ID,Bug Resolution Date by Jira,Classes Touched by Last Fix,Earliest AV,Version of Ticket Creation,Version of Fix,Ticket Creation Date,Fix Date (of most recent commit),Hash (of most recent commit),Git Directory");
				                bugWriter.append("\n");
                        for ( j = 0; j < bugs.size(); j++) {
                           if ( closedBugs.get(bugs.get(j)) != null && closedBugsAndAV.get(bugs.get(j)) != null
                            && closedBugsAndAV.get(bugs.get(j)) <= closedBugsAndCreation.get(bugs.get(j))
                            && closedBugsAndAV.get(bugs.get(j)) <= closedBugs.get(bugs.get(j)) &&
                            !(closedBugsAndAV.get(bugs.get(j)) == closedBugsAndCreation.get(bugs.get(j)) &&
                            closedBugsAndAV.get(bugs.get(j)) == closedBugs.get(bugs.get(j)))) {
                               write = bugs.get(j) + "," +
                                closedBugsAndResolution.get(bugs.get(j)) + "," +
                                closedBugsAndClasses.get(bugs.get(j)) + "," +
                                closedBugsAndAV.get(bugs.get(j)).toString() + "," +
                                closedBugsAndCreation.get(bugs.get(j)).toString() + "," +
                                closedBugs.get(bugs.get(j)).toString() + ","+
                                closedBugsAndCreationDate.get(bugs.get(j)) + "," +
                                closedBugsAndFix.get(bugs.get(j)) + "," +
                                closedBugsAndHash.get(bugs.get(j))+ "," +
                                closedBugsAndDirectory.get(bugs.get(j)) + "\n";
                                bugWriter.append(write);
                            }
                         }
                      } catch (Exception e) {
                         System.out.println("Error in csv writer");
                         e.printStackTrace();
                      } finally {
                         try {
                            bugWriter.flush();
                            bugWriter.close();
                         } catch (IOException e) {
                            System.out.println("Error while flushing/closing fileWriter !!!");
                            e.printStackTrace();
                         }
                      }
                     FileWriter fixWriter = null;
				             try {
                        fixWriter = null;
                        String outname = token[0] + "BugFixes.csv";
				                //Name of CSV for output
				                fixWriter = new FileWriter(outname);
				                //Header for CSV
				                fixWriter.append("Bug ID,Bug Fix Commit Hash,Date of Commit");
				                fixWriter.append("\n");
                        for ( j = 0; j < bugs.size(); j++) {
                           if (bugToFixes.get(bugs.get(j)) != "" ) {
                           String [] fixTokens = bugToFixes.get(bugs.get(j)).split("\n");
                           for (int f = 0; f < fixTokens.length; f++ ) {
                              String datePartfix = fixTokens[f].substring(0, 25);
                              String hashfix = fixTokens[f].substring(25);
                              fixWriter.append(bugs.get(j) + "," + hashfix + "," + datePartfix + "\n");
                           }
                         }
                         }
                      } catch (Exception e) {
                         System.out.println("Error in csv writer");
                         e.printStackTrace();
                      } finally {
                         try {
                            fixWriter.flush();
                            fixWriter.close();
                         } catch (IOException e) {
                            System.out.println("Error while flushing/closing fileWriter !!!");
                            e.printStackTrace();
                         }
                      }
                  }
               }
            }
         } catch (Exception e) {
            System.out.println("Error in csv writer");
            e.printStackTrace();
         } finally {
            try {
               fileWriter.flush();
               fileWriter.close();
               fileWriter2.flush();
               fileWriter2.close();
            } catch (IOException e) {
               System.out.println("Error while flushing/closing fileWriter !!!");
               e.printStackTrace();
            }
         }
      } catch (IOException e) {
         e.printStackTrace();
      }
   }
}
