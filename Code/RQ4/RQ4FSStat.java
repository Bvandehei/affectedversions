import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.math.*;

public class RQ4FSStat {

   public static void main(String[] args) throws IOException, InterruptedException {
      String csvSplitBy = ",";
      String methodsb = "Simple,SZZ_B+,SZZ_U+,SZZ_RA+,SZZ_B,SZZ_U,SZZ_RA,Proportion_ColdStart,Proportion_Increment,Proportion_MovingWindow";
      String [] methodNames = methodsb.split(csvSplitBy);
      HashMap<String,String> actual = new HashMap<String,String>();
      HashMap<String,String> methods = new HashMap<String,String>();
      try (BufferedReader br = new BufferedReader(new FileReader("SelectedAttributes.csv"))) {
         String lineProjects = br.readLine();
         while ( (lineProjects = br.readLine()) != null ) {
           String[] token = lineProjects.split(csvSplitBy);
           if (token[2].equals("Actual")) {
             String features = "";
             for (int f = 3; f < token.length; f++) {
               features += token[f] + ",";
             }
             actual.put(token[0] + "," + token[1], features);
           } else {
             String features = "";
             for (int f = 3; f < token.length; f++) {
               features += token[f] + ",";
             }
             methods.put(token[0] + "," + token[1] + "," + token[2], features);
           }

         }
      } catch (IOException e) {
         e.printStackTrace();
      }

methods.entrySet().forEach(entry->{
   System.out.println(entry.getKey() + " " + entry.getValue());
});
     //project,feature or all,feature
     HashMap<String, Double> projfeature2used = new HashMap<String,Double>();
     //project or all (num classifications)
     HashMap<String, Double> proj2total = new HashMap<String,Double>();
     String featuresS = "LOC,LOC_touched,NR,NFix,NAuth,LOC_added,MAX_LOC_added,AVG_LOC_added,Churn,MAX_Churn,AVG_Churn,ChgSetSize,MAX_ChgSet,AVG_ChgSet,Age,WeightedAge";
     String [] features = featuresS.split(csvSplitBy);
     FileWriter fileWriter = null;
     try {
		    fileWriter = new FileWriter("SelectedAttributesStatistics.csv");
				fileWriter.append("project,release,method,TP,TN,FP,FN,precision,recall,F1,MCC,kappa\n");
        for (Map.Entry el : actual.entrySet()) {
            String key = (String)el.getKey();
            String [] keytok = key.split(",");
           if (proj2total.containsKey(keytok[0]))
             proj2total.put(keytok[0],proj2total.get(keytok[0]) +1.0);
           else
             proj2total.put(keytok[0],1.0);
            String value = (String)el.getValue();
            String [] actualFSb = value.split(csvSplitBy);
            List<String> actualFS = new ArrayList<String>();
	          actualFS = Arrays.asList(actualFSb);
            for (int m = 0; m< methodNames.length; m++) {
              String methodkey = key + "," + methodNames[m];
              String [] keymtok = methodkey.split(",");
              if (methods.containsKey(methodkey)){
                Long TP = 0L, TN = 0L, FP = 0L, FN = 0L;
                String [] methodsFSb = methods.get(methodkey).split(csvSplitBy);
                List<String> methodFS = new ArrayList<String>();
    	          methodFS = Arrays.asList(methodsFSb);
                for (int f = 0; f< features.length; f++){
                   if (m == 0) {
                     if (actualFS.contains(features[f])) {
                       if (projfeature2used.containsKey(keytok[0]+ "," + features[f]))
                         projfeature2used.put(keytok[0]+ "," + features[f],projfeature2used.get(keytok[0]+ "," + features[f]) +1.0);
                       else
                         projfeature2used.put(keytok[0]+ "," + features[f],1.0);
                     }
                   }

					         if (actualFS.contains(features[f])) {
					            if (methodFS.contains(features[f]))
					               TP++;
					            else
					               FN++;
					         }
					         else {
					            if (methodFS.contains(features[f]))
					               FP++;
					            else
					               TN++;
					         }
}
                  Double precision = 0.0;
                  Double recall = 0.0;
                  if (TP != 0) {
						         precision = (double)TP /(double)(TP + FP);
						         recall = (double)TP /(double)(TP + FN);
                  }
						      Double F1 = 0.0;
                  if ((precision * recall) != 0)
                    F1 = (double)(precision * recall *2) / (double)(precision + recall);
						      Double MCC = 0.0;
                  if ( (TP * TN - FP * FN) != 0)
                    MCC = (double)(TP * TN - FP * FN) / (Math.sqrt((TP + FP) * (FN + TN) * (FP + TN) * (TP + FN)));
						      Double observed = (double)(TP + TN) / (double)(TP+ FN + FP + TN);
						      Double a = (double)((TP + FN) * (TP + FP)) / (double)(TP+ FN + FP + TN);
						      Double b = (double)((FP + TN) * (FN + TN)) / (double)(TP+ FN + FP + TN);
						      Double expected = (a + b) / (double)(TP+ FN + FP + TN);
						      Double kappa = (observed - expected)/(1 - expected);

						      fileWriter.append( methodkey + "," + TP.toString()
                             + "," + TN.toString() + "," + FP.toString() + "," + FN.toString() + ","
                             + precision.toString() + "," + recall.toString()
						                 + "," + F1.toString() +"," + MCC.toString() + "," + kappa.toString() + "\n");

            }
          }
        }
     } catch (Exception e) {
        System.out.println("Error in csv writer");
        e.printStackTrace();
     } finally {
        try {
           fileWriter.flush();
           fileWriter.close();
        } catch (IOException e) {
           System.out.println("Error while flushing/closing fileWriter !!!");
           e.printStackTrace();
        }
     }

     FileWriter fileWriter2 = null;
     try {
		    fileWriter2 = new FileWriter("PercentFeatureSelected.csv");
        fileWriter2.append("project,");
        for (int f = 0; f< features.length; f++){
          fileWriter2.append(features[f]+",");
        }
        fileWriter2.append("\n");
        for (Map.Entry el : proj2total.entrySet()) {
          String proj = (String)el.getKey();
          fileWriter2.append(proj+",");
          for (int f = 0; f< features.length; f++){
            if (projfeature2used.containsKey(proj+","+features[f])){
              fileWriter2.append((double)(projfeature2used.get(proj+","+features[f])/(double)el.getValue())+",");
            } else {
              fileWriter2.append("0,");
            }
          }
          fileWriter2.append("\n");
        }
     } catch (Exception e) {
        System.out.println("Error in csv writer");
        e.printStackTrace();
     } finally {
        try {
           fileWriter2.flush();
           fileWriter2.close();
        } catch (IOException e) {
           System.out.println("Error while flushing/closing fileWriter !!!");
           e.printStackTrace();
        }
     }
   }
}
