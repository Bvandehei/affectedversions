import org.apache.commons.math3.stat.correlation.SpearmansCorrelation;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.math.*;

public class RQ4SCActual {
    public static boolean isNumeric(String strNum) {
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }
   public static void main(String[] args) throws IOException, InterruptedException {
      String csvSplitBy = ",";

      String title2 = "Version,File Name,LOC,LOC_touched,NR,NFix,NAuth,LOC_added,MAX_LOC_added,AVG_LOC_added,Churn,MAX_Churn,AVG_Churn,ChgSetSize,MAX_ChgSet,AVG_ChgSet,Age,WeightedAge,Buggy";
      String [] features = title2.split(csvSplitBy);
      try (BufferedReader br = new BufferedReader(new FileReader("../RQ1/ProjectDetails.csv"))) {
         String lineProjects = br.readLine();
         while ( (lineProjects = br.readLine()) != null ) {
           String[] token = lineProjects.split(csvSplitBy);
           String project = token[0];
           int versions = Integer.parseInt(token[7]);
           String path =  "";
   
             for (Integer r = 2; r < versions; r++) {
               for (Integer c = 2; c<18; c++) {
                 ArrayList<Double> items = new ArrayList<Double>();
                 ArrayList<Double> def = new ArrayList<Double>();
                 int numYA=0;

                 ArrayList<Double> itemsA = new ArrayList<Double>();
                 ArrayList<Double> defA = new ArrayList<Double>();
				         try (BufferedReader brMetrics = new BufferedReader(new FileReader("../RQ3/Complete/"+ project +"_Actual_Complete.csv"))) {
				            String linefromcsv = brMetrics.readLine();
                    Boolean gt = false;
				            while ( (linefromcsv = brMetrics.readLine()) != null && gt == false ) {
				               String[] tokenMetrics = linefromcsv.split(csvSplitBy);
				               if (tokenMetrics.length != 19)
                         System.out.println("ERROR LINE NOT 19");
                       else if (Integer.parseInt(tokenMetrics[0]) <= r){
                          if (!(isNumeric(tokenMetrics[3]) || isNumeric(tokenMetrics[4]))){

                          }
                          else if (isNumeric(tokenMetrics[2])){
                            itemsA.add(Double.parseDouble(tokenMetrics[c]));
                            if (tokenMetrics[18].equals("Yes")){
                              defA.add(1.0);
                              numYA++;
                            }
                            else if (tokenMetrics[18].equals("No"))
                              defA.add(0.0);
                            else
                              System.out.println("not a buggy column");
                          } else {
                            if (c != 17)
                              itemsA.add(Double.parseDouble(tokenMetrics[c+1]));
                            else
                              itemsA.add(0.0);
                            if (tokenMetrics[18].equals("Yes")){
                              numYA++;
                              defA.add(1.0);
                            }
                            else if (tokenMetrics[18].equals("No"))
                              defA.add(0.0);
                            else
                              System.out.println("not a buggy column");
                          }
                       } else {
                         gt = true;
                       }
				            }
				         } catch (IOException e) {
				           e.printStackTrace();
				         }
                 if (numYA>0 ){

                 double[] a3 = new double[itemsA.size()];
                 for (int i = 0; i < a3.length; i++) {
                   a3[i] = itemsA.get(i);
                 }
                 double[] a4 = new double[defA.size()];
                 for (int i = 0; i < a4.length; i++) {
                   a4[i] = defA.get(i);
                 }

                 SpearmansCorrelation scA = new SpearmansCorrelation();
                 double correlationA = scA.correlation(a3,a4);

                  System.out.println(project+ "," + r+ ","  + features[c]+","+ correlationA);
                 }
               }
             }

        }
      } catch (IOException e) {
         e.printStackTrace();
      }
   }
}
