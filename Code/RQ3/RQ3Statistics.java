import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.math.*;

public class RQ3Statistics {

   public static void main(String[] args) throws IOException, InterruptedException {
      String csvSplitBy = ",";
      String title = "Project Key,Bug ID,Bug Order,Version ID,Version Name,Version Index,Simple,Proportion_ColdStart,Proportion_Increment,Proportion_MovingWindow,SZZ_U,SZZ_B,SZZ_RA,Proportion_ColdStart+,Proportion_Increment+,Proportion_MovingWindow+,SZZ_U+,SZZ_B+,SZZ_RA+";
      System.out.println("project,method,TP,TN,FP,FN,precision,recall,F1,MCC,kappa");
      String [] titleSplit = title.split(csvSplitBy);
      try (BufferedReader br = new BufferedReader(new FileReader("../RQ1/TopProjects.csv"))) {
         String lineProjects;
         while ( (lineProjects = br.readLine()) != null ) {
           String[] token = lineProjects.split(csvSplitBy);
           String project = token[0];
           String path =  "";
           for (Integer column = 6; column < titleSplit.length; column++) {
				      try (BufferedReader brMethods = new BufferedReader(new FileReader("Complete/"+ project +"_" + titleSplit[column] + "_Complete.csv"))) {
				        try (BufferedReader brActual = new BufferedReader(new FileReader("Complete/" +project +"_Actual_Complete.csv"))) {
			            String linefrommethods = brMethods.readLine();
			            String linefromactual = brActual.readLine();
                  Long TP = 0L, TN = 0L, FP = 0L, FN = 0L;
			            while ( (linefrommethods = brMethods.readLine()) != null
                      &&  (linefromactual = brActual.readLine()) != null) {
			               String[] tokenmethods = linefrommethods.split(csvSplitBy);
			               String[] tokenactual = linefromactual.split(csvSplitBy);
                     if (!tokenmethods[1].equals(tokenactual[1])) {
                       System.out.println("Mismatch in file name");
                       System.exit(-1);
                     }
                     if (tokenactual[tokenactual.length-1].equals("Yes")) {
                       if (tokenmethods[tokenmethods.length-1].equals("Yes"))
                         TP++;
                       else
                         FN++;
                     } else {
                       if (tokenmethods[tokenmethods.length-1].equals("Yes"))
                         FP++;
                       else
                         TN++;
                     }
			            }

                  BigDecimal TPb =  BigDecimal.valueOf(TP);
                  BigDecimal FPb = BigDecimal.valueOf(FP);
                  BigDecimal TNb = BigDecimal.valueOf(TN);
                  BigDecimal FNb =  BigDecimal.valueOf(FN);
                  BigDecimal zb =  BigDecimal.valueOf(0);
                  Double precision = (double)TP /(double)(TP + FP);
                  Double recall = (double)TP /(double)(TP + FN);
                  Double F1 = (double)(precision * recall *2) / (double)(precision + recall);
                  BigDecimal MCC;
                  if (((TPb.add(FPb)).multiply(FNb.add(TNb)).multiply(FPb.add(TNb)).multiply(TPb.add(FNb))).compareTo(zb) == 0 )
                    MCC = (TPb.multiply(TNb).subtract(FPb.multiply(FNb)));
                  else
                    MCC = (TPb.multiply(TNb).subtract(FPb.multiply(FNb))).divide(((TPb.add(FPb)).multiply(FNb.add(TNb)).multiply(FPb.add(TNb)).multiply(TPb.add(FNb))).sqrt(MathContext.DECIMAL128), MathContext.DECIMAL128);

                  Double observed = (double)(TP + TN) / (double)(TP+ FN + FP + TN);
                  Double a = (double)((TP + FN) * (TP + FP)) / (double)(TP+ FN + FP + TN);
                  Double b = (double)((FP + TN) * (FN + TN)) / (double)(TP+ FN + FP + TN);
                  Double expected = (a + b) / (double)(TP+ FN + FP + TN);
                  Double kappa = (observed - expected)/(1 - expected);
                  System.out.println(project + "," + titleSplit[column] + ","
                                    + TP + "," + TN + "," + FP + "," + FN + ","
                                    + precision + "," + recall + "," + F1 + "," + MCC + "," + kappa);
				        } catch (IOException e) {
				           e.printStackTrace();
				        }
				      } catch (IOException e) {
				         e.printStackTrace();
				      }
           }
         }
      } catch (IOException e) {
         e.printStackTrace();
      }
   }
}
