#!/bin/bash

FILES=*.csv
for f in $FILES
do
  newname="Ordered"
  newname+="$f"
  (head -n1 $f && sort -k8 -t, <(tail -n+2 $f)) > $newname
done
